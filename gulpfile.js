var gulp = require('gulp'),
    uglify = require('gulp-uglify');

gulp.task('uglifyJS', function() {
  gulp.src('*script.js')
  .pipe(uglify())
  .pipe(gulp.dest("production"));
});

gulp.task("watch", function() {
  gulp.watch('*script.js', ["uglifyJS"]);
});

gulp.task('default', ['uglifyJS', 'watch']);
