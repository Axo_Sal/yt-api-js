var clientId = 'your client id';
var scopes = ['https://www.googleapis.com/auth/youtube'];

function handleClientLoad() {
  window.setTimeout(checkAuth,1);
}
function checkAuth() {
  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
}
function handleAuthResult(authResult) {
  var authorizeButton = document.getElementById('authorize-button');
  if (authResult && !authResult.error) {

    document.getElementById("postAuth").style.display = 'block';
    document.getElementById("preAuth").style.display = 'none';
    document.getElementById("aff").style.display = 'block';
    document.querySelector("h2").style.display = "none";
    document.getElementById("loadingAnimation").style.display = 'none';
    makeApiCall();

  } else {
    document.getElementById("postAuth").style.display = 'none';
    document.getElementById("preAuth").style.display = 'block';
    document.getElementById("aff").style.display = 'block';
    document.querySelector("h2").style.display = "none";
    document.getElementById("loadingAnimation").style.display = 'none';
    authorizeButton.onclick = handleAuthClick;
  }
}
function handleAuthClick(event) {
  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
  return false;
}

document.getElementById('signOut').onclick = function() {
  var token = gapi.auth.getToken();
  if (token) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', "https://accounts.google.com/o/oauth2/revoke?token=" + token.access_token);
    xhr.send();
  }
  gapi.auth.setToken(null);
};

// Load the API and make an API call.  Display the results on the screen.
function makeApiCall() {
  gapi.client.load('youtube', 'v3', function() {
    var request = gapi.client.youtube.activities.list({
      'part': 'contentDetails',
      'home': true,
      'maxResults': 50,
      'fields': 'items/contentDetails'
    });
    request.execute(function(resp) {

      var whatHappensDiv = document.getElementById('whatHappens');
      whatHappensDiv.innerHTML = "Retrieves videos.";
      document.getElementById('playlist').innerHTML = "Wait...";
      var listOfIds = [];
      for ( var i=0; i<resp.items.length; i++ ) {

          if ( resp.items[i].contentDetails.upload !== undefined ) {
              listOfIds.push(resp.items[i].contentDetails.upload.videoId);
          }
          if ( resp.items[i].contentDetails.like !== undefined ) {
            listOfIds.push(resp.items[i].contentDetails.like.resourceId.videoId);
          }
          if ( resp.items[i].contentDetails.recommendation !== undefined ) {
            listOfIds.push(resp.items[i].contentDetails.recommendation.resourceId.videoId);
          }
          if ( resp.items[i].contentDetails.bulletin !== undefined ) {
            listOfIds.push( resp.items[i].contentDetails.bulletin.resourceId.videoId );
          }
          if ( resp.items[i].contentDetails.playlistItem !== undefined ) {
            listOfIds.push( resp.items[i].contentDetails.playlistItem.resourceId.videoId );
          }

      }

      function eliminateDuplicates(arr) {
        var i,
            len=arr.length,
            out=[],
            obj={};

        for (i=0;i<len;i++) {
          obj[arr[i]]=0;
        }
        for (i in obj) {
          out.push(i);
        }
        return out;
      }

      var listOfIdsWithoutDuplicates = eliminateDuplicates( listOfIds );
      var playlistTitle = "youtube api with js";

      var playlistId;
      function createPlaylist() {
        var request = gapi.client.youtube.playlists.insert({
          part: 'snippet,status',
          resource: {
            snippet: {
              title: playlistTitle,
              description: 'read the title yaow!'
            },
            status: {
              privacyStatus: 'unlisted'
            }
          }
        });
        request.execute(function(response) {
          var result = response.result;
          if (result) {
            playlistId = result.id;
            whatHappensDiv.innerHTML = "Adding videos to the playlist.";
            console.log(listOfIdsWithoutDuplicates.length);
            addToPlaylist( listOfIdsWithoutDuplicates[0], playlistId );
            var i = 1;
            var interval = setInterval(function() {
                addToPlaylist( listOfIdsWithoutDuplicates[i], playlistId );
                if ( i === listOfIdsWithoutDuplicates.length-1 ) {
                  var FirstVideo = listOfIdsWithoutDuplicates[listOfIdsWithoutDuplicates.length-1];
                  whatHappensDiv.style.display = "none";
                  document.getElementById('playlist').innerHTML = '<iframe id="ytplayer" type="text/html" width="462" height="260" src="https://www.youtube.com/embed/'+FirstVideo+'?list='+playlistId+'&autoplay=1" frameborder="0" allowfullscreen>';
                  // document.getElementById('playlist').innerHTML = "<p> Here's the link to your generated YouTube playlist: <a target='_blank' href='https://www.youtube.com/watch?v="+FirstVideo+"&list="+playlistId+"'>youtube.com/watch?v=...</a> </p>";
                  clearInterval(interval);
                }
                i++;
            }, 350);
          } else {
            console.log("fail");
          }
        });
      }

      function addToPlaylist(id, pId1) {
        var details = {
          videoId: id,
          kind: 'youtube#video'
        };
        var request = gapi.client.youtube.playlistItems.insert({
          part: 'snippet',
          resource: {
            snippet: {
              playlistId: pId1,
              resourceId: details,
              position: 0
            }
          }
        });
        request.execute();
      }

      function getPlaylists() {
        var request = gapi.client.youtube.playlists.list({
          part: 'snippet',
          mine: true,
          maxResults: 50,
          fields: 'items(id,snippet)'
        });
        var playlists = [];
        request.execute(function(response) {

          if ('error' in response) {
            document.getElementById("loadingAnimation").style.display = "none";
            whatHappensDiv.innerHTML = 'You need to create a youtube channel to be able to make playlists.&nbsp;<a target="_blank" href="https://support.google.com/youtube/answer/1646861?topic=3024170&hl=en">Create it here</a>&nbsp;and then reload this page. <br> If you see this message although you already have a youtube channel, it\'s due to network error. Reload this page to try again.';
          }
          else {

            for ( var i=0; i<response.items.length; i++ ) {
              playlists.push( response.items[i].snippet.title );
            }

            if ( playlists.indexOf( playlistTitle ) > -1 ) {

              var pId = response.items[ playlists.indexOf( playlistTitle ) ].id;

              gapi.client.youtube.playlists.delete({
                id: pId
              }).execute();

              createPlaylist();

            }

            if ( playlists.indexOf( playlistTitle ) === -1 ) {
              createPlaylist();
            }

          }

        });
      }

      getPlaylists();

    }); // end of request execute function response

  }); // end of youtube v3 client api load

} // end of makeApiCall();
